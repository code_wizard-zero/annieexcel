﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using System.Windows;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace AnnieExcel
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var instance = new Application();

            var openDialog = new OpenFileDialog
            {
                Multiselect = true,
                AddExtension = false,
                CheckFileExists = false,
                CheckPathExists = false,
                DefaultExt = "xlsx"
            };

            openDialog.ShowDialog();

            var saveDialog = new SaveFileDialog
            {
                AddExtension = false,
                CheckFileExists = false,
                CheckPathExists = false,
                CreatePrompt = false,
                DefaultExt = "xlsx",
                FileName = "report"
            };
            ;
            saveDialog.ShowDialog();

            _Workbook destWorkbook = instance.Workbooks.Add();
            var destSheet = destWorkbook.ActiveSheet as _Worksheet;
            destWorkbook.SaveAs(saveDialog.FileName);
            if (destSheet == null)
                return;
            var index = 1;

            foreach (var fileName in openDialog.FileNames)
            {
                var sourceWorkbook = instance.Workbooks.Open(fileName);
                var sourceSheet = sourceWorkbook.ActiveSheet as _Worksheet;
                if (sourceSheet == null) continue;
                var count = sourceSheet.UsedRange.Rows.Count;

                var destEnd = 0;

                if (fileName.Trim().ToLower().EndsWith("_ftype.xls"))
                {
                    destEnd = LoadType(index, count, sourceSheet, destSheet);
                }
                else
                {
                    destEnd = LoadProps(index, count, sourceSheet, destSheet);
                }

                sourceWorkbook.Close();

                destWorkbook.Save();
                index = destEnd + 1;
            }

            destWorkbook.Save();

            instance.Quit();
            MessageBox.Show("Report Generated! :)");

            Current.Shutdown();
        }

        protected int Load(int header, string column, int index, int count, _Worksheet sourceSheet, _Worksheet destSheet)
        {
            var destStart = index;
            var destEnd = index + count - header;
            var sourceEnd = count;

            var sourceRowStart = "A" + header;
            var sourceRowEnd = column + sourceEnd;
            var destRowStart = "A" + destStart;
            var destRowEnd = column + destEnd;

            sourceSheet.Range[sourceRowStart, sourceRowEnd].Copy(destSheet.Range[destRowStart, destRowEnd]);
            return destEnd;

        }

        protected int LoadProps(int index, int count, _Worksheet sourceSheet, _Worksheet destSheet)
        {
            return Load(7, "G", index, count, sourceSheet, destSheet);
        }

        protected int LoadType(int index, int count, _Worksheet sourceSheet, _Worksheet destSheet)
        {
            return Load(11, "G", index, count, sourceSheet, destSheet);
        }
    }
}
